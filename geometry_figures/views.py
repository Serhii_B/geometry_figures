from django.shortcuts import render
from .form import ThreePeaksForm, FourPeaksForm,\
    FivePeaksFormDot, FivePeaksForm
from .models import Triangle, Rectangle, Pentagon


def index(request):
    return render(request, "base.html")


def methods(request):
    return render(request, "methods.html")


def receiving_data_controller(clsform, template, target_template):
    def decorator(func):

        def inner(request, *args, **kwargs):
            if request.method == "POST":
                form = clsform(request.POST)
                if form.is_valid():
                    res = func(*form.cleaned_data.values())
                    return render(request, target_template, res)
            else:
                form = clsform()

            return render(request, template, {'form': form})

        return inner
    return decorator


@receiving_data_controller(ThreePeaksForm, "base_form.html",
                           "response_generator.html")
def show_coordinates_triangle(a0, a1, b0, b1, c0, c1):
    triangle = Triangle((a0, a1), (b0, b1), (c0, c1))
    res = {"message": triangle.show_coordinates()}
    return res


@receiving_data_controller(ThreePeaksForm, "base_form.html",
                           "response_generator.html")
def count_angels_triangle(a0, a1, b0, b1, c0, c1):
    triangle = Triangle((a0, a1), (b0, b1), (c0, c1))
    res = {"message": triangle.count_angles()}
    return res


@receiving_data_controller(FourPeaksForm, "base_form.html",
                           "triangle/triangle_show_coordinates.html")
def check_center_triangle(a0, a1, b0, b1, c0, c1, d0, d1):
    triangle = Triangle((a0, a1), (b0, b1), (c0, c1))
    res = {"message": triangle.check_center((d0, d1))}
    return res


@receiving_data_controller(FourPeaksForm, "base_form.html",
                           "response_generator.html")
def check_dot_in_figure_triangle(a0, a1, b0, b1, c0, c1, d0, d1):
    triangle = Triangle((a0, a1), (b0, b1), (c0, c1))
    res = {"message": triangle.check_dot_in_figure((d0, d1))}
    return res


@receiving_data_controller(FourPeaksForm, "base_form.html",
                           "response_generator.html")
def show_coordinates_rectangle(a0, a1, b0, b1, c0, c1, d0, d1):
    rectangle = Rectangle((a0, a1), (b0, b1), (c0, c1), (d0, d1))
    res = {"message": rectangle.show_coordinates()}
    return res


@receiving_data_controller(FourPeaksForm, "base_form.html",
                           "response_generator.html")
def count_angels_rectangle(a0, a1, b0, b1, c0, c1, d0, d1):
    rectangle = Rectangle((a0, a1), (b0, b1), (c0, c1), (d0, d1))
    res = {"message": rectangle.count_angles()}
    return res


@receiving_data_controller(FivePeaksForm, "base_form.html",
                           "response_generator.html")
def check_center_rectangle(a0, a1, b0, b1, c0, c1, d0, d1, e0, e1):
    rectangle = Rectangle((a0, a1), (b0, b1), (c0, c1), (d0, d1))
    res = {"message": rectangle.check_center((e0, e1))}
    return res


@receiving_data_controller(FivePeaksForm, "base_form.html",
                           "response_generator.html")
def check_dot_in_figure_rectangle(a0, a1, b0, b1, c0, c1, d0, d1, e0, e1):
    rectangle = Rectangle((a0, a1), (b0, b1), (c0, c1), (d0, d1))
    res = {"message": rectangle.check_center((e0, e1))}
    return res


@receiving_data_controller(FivePeaksForm, "base_form.html",
                           "response_generator.html")
def show_coordinates_pentagon(a0, a1, b0, b1, c0, c1, d0, d1, e0, e1):
    pentagon = Pentagon((a0, a1), (b0, b1), (c0, c1), (d0, d1), (e0, e1))
    res = {"message": pentagon.show_coordinates()}
    return res


@receiving_data_controller(FivePeaksForm, "base_form.html",
                           "response_generator.html")
def count_angels_pentagon(a0, a1, b0, b1, c0, c1, d0, d1, e0, e1):
    pentagon = Pentagon((a0, a1), (b0, b1), (c0, c1), (d0, d1), (e0, e1))
    res = {"message": pentagon.count_angles()}
    return res


@receiving_data_controller(FivePeaksFormDot,
                           "base_form.html",
                           "response_generator.html")
def check_center_pentagon(a0, a1, b0, b1, c0, c1, d0, d1, e0, e1, f0, f1):
    pentagon = Pentagon((a0, a1), (b0, b1), (c0, c1), (d0, d1), (e0, e1))
    res = {"message": pentagon.check_center((f0, f1))}
    return res


@receiving_data_controller(FivePeaksFormDot,
                           "base_form.html",
                           "response_generator.html")
def check_dot_in_figure_pentagon(a0, a1, b0, b1, c0, c1, d0, d1, e0, e1, f0,
                                 f1):
    pentagon = Pentagon((a0, a1), (b0, b1), (c0, c1), (d0, d1), (e0, e1))
    res = {"message": pentagon.check_dot_in_figure((f0, f1))}
    return res
