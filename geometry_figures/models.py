from abc import ABC, abstractmethod

from shapely.geometry import Polygon, Point
from shapely.geometry.polygon import LinearRing


class GeometricFigure(ABC):
    class Meta:
        abstract = True

    @abstractmethod
    def count_angles(self):
        pass


class Triangle(GeometricFigure):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def show_coordinates(self):
        resp = "I am triangle and my coordinates are:\n Point A is X:{}" \
               "Y:{}, \n Point B is X:{}  Y:{}, \n Point C is X:{}  Y:{}"\
            .format(self.a[0], self.a[1], self.b[0], self.b[1],
                    self.c[0], self.c[1])

        return resp

    def count_angles(self):
        resp = "I have 3 angles"
        return resp

    def check_center(self, value):
        val1 = (self.a[0] + self.b[0] + self.c[0]) / 3
        val2 = (self.a[1] + self.b[1] + self.c[1]) / 3
        if value[0] == val1 and value[1] == val2:
            return True
        else:
            return False

    def check_dot_in_figure(self, dot):
        def area_func(x1, y1, x2, y2, x3, y3):
            return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
                        + x3 * (y1 - y2)) / 2.0)

        area = area_func(self.a[0], self.a[1], self.b[0],
                         self.b[1], self.c[0], self.c[1])

        area_dot1 = area_func(dot[0], dot[1], self.b[0],
                              self.b[1], self.c[0], self.c[1])
        area_dot2 = area_func(self.a[0], self.a[1], dot[0],
                              dot[1], self.c[0], self.c[1])
        area_dot3 = area_func(self.a[0], self.a[1], self.b[0],
                              self.b[1], dot[0], dot[1])

        dot_sum = area_dot1 + area_dot2 + area_dot3

        if area == dot_sum:
            return True

        else:
            return False


class Rectangle(GeometricFigure):

    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def show_coordinates(self):
        resp = "I am Rectangle and my coordinates are:" \
               "\n Point A is X:{}  Y:{}, \n Point B is X:{}"\
               "Y:{}, \n Point C is X:{}  Y:{},"\
               " \n Point D is X:{}   Y:{}"\
            .format(self.a[0], self.a[1], self.b[0], self.b[1],
                    self.c[0], self.c[1], self.d[0], self.d[1])
        return resp

    def count_angles(self):
        return "I have 4 angles"

    def check_center(self, value):
        val1 = (self.a[0] + self.b[0] + self.c[0] + self.d[0]) / 4
        val2 = (self.a[1] + self.b[1] + self.c[1] + self.d[1]) / 4
        if value[0] == val1 and value[1] == val2:
            return True
        else:
            return False

    def check_dot_in_figure(self, dot):
        min_x = min(self.a[0], self.b[0], self.c[0], self.d[0])
        max_x = max(self.a[0], self.b[0], self.c[0], self.d[0])

        min_y = min(self.a[1], self.b[1], self.c[1], self.d[1])
        max_y = max(self.a[1], self.b[1], self.c[1], self.d[1])

        if min_x <= dot[0] <= max_x and min_y <= dot[1] <= max_y:
            return True
        else:
            return False


class Pentagon(GeometricFigure):

    def __init__(self, a, b, c, d, e):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.e = e

    def count_angles(self):
        return "I have 5 angles"

    def show_coordinates(self):
        resp = "I am Pentagon and my coordinates are:\n Point A is X:{}" \
               "Y:{}, \n Point B is X:{}  Y:{}, \n Point C is X:{}  Y:{},"\
               " \n Point D is X:{}   Y:{}, \n Point E is X:{}   Y:{}"\
            .format(self.a[0], self.a[1], self.b[0], self.b[1], self.c[0],
                    self.c[1], self.d[0], self.d[1], self.e[0], self.e[1])
        return resp

    def check_center(self, value):
        val1 = (self.a[0] + self.b[0] + self.c[0] + self.d[0] + self.e[0]) / 5
        val2 = (self.a[1] + self.b[1] + self.c[1] + self.d[1] + self.e[1]) / 5
        if value[0] == val1 and value[1] == val2:
            return True
        else:
            return False

    def check_dot_in_figure(self, dot):
        pentagon = Polygon([[self.a[0], self.a[1]], [self.b[0], self.b[1]],
                            [self.c[0], self.c[1]],
                            [self.d[0], self.d[1]], [self.e[0], self.e[1]]])

        linear = LinearRing(list(pentagon.exterior.coords))
        point = Point(dot[0], dot[1])

        if pentagon.contains(point) or linear.contains(point):
            return True
        else:
            return False
