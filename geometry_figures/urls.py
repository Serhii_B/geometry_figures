from django.urls import path
from . import views

urlpatterns = [
    path('',  views.index),

    path('triangle_methods/', views.methods),
    path('triangle_methods/show_coordinates/',
         views.show_coordinates_triangle),
    path('triangle_methods/count_angels/', views.count_angels_triangle),
    path('triangle_methods/check_center/',
         views.check_center_triangle),
    path('triangle_methods/check_dot_in_figure/',
         views.check_dot_in_figure_triangle),

    path('rectangle_methods/', views.methods),
    path('rectangle_methods/show_coordinates/',
         views.show_coordinates_rectangle),
    path('rectangle_methods/count_angels/',
         views.count_angels_rectangle),
    path('rectangle_methods/check_center/',
         views.check_center_rectangle),
    path('rectangle_methods/check_dot_in_figure/',
         views.check_dot_in_figure_rectangle),

    path('pentagon_methods/', views.methods),
    path('pentagon_methods/show_coordinates/',
         views.show_coordinates_pentagon),
    path('pentagon_methods/count_angels/', views.count_angels_pentagon),
    path('pentagon_methods/check_center/', views.check_center_pentagon),
    path('pentagon_methods/check_dot_in_figure/',
         views.check_dot_in_figure_pentagon),

]
