from django import forms


class ThreePeaksForm(forms.Form):
    A1 = forms.IntegerField()
    A2 = forms.IntegerField()
    B1 = forms.IntegerField()
    B2 = forms.IntegerField()
    C1 = forms.IntegerField()
    C2 = forms.IntegerField()


class FourPeaksForm(ThreePeaksForm):
    D1 = forms.IntegerField()
    D2 = forms.IntegerField()


class FivePeaksForm(FourPeaksForm):
    E1 = forms.IntegerField()
    E2 = forms.IntegerField()


class FivePeaksFormDot(FivePeaksForm):
    F1 = forms.IntegerField()
    F2 = forms.IntegerField()
