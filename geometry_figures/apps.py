from django.apps import AppConfig


class GeometryFiguresConfig(AppConfig):
    name = 'geometry_figures'
