FROM python:3.6.9
WORKDIR /usr/src/django_geometry

COPY . /usr/src/django_geometry

RUN pip install --upgrade pip

RUN pip install -r requirements.txt

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000", "--noreload"]
